## Teach Your Tomcat Embedding Tricks

March 28 2013 Toronto Java Usres Group Meeting

This presentation will provide you with an excellent intorduction to three different ways you can embedded Tomcat 7.0.x, along with some tricks for a more productive Tomcat, git & maven workflow. The presentation is a sequence of demos, source code and slides will be available on bitbucket. No previous Tomcat experience required.

Slides and code samples for the presentation are here

Video of presentation at https://vimeo.com/63206212/ 
