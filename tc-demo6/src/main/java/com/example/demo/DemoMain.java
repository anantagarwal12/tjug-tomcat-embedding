package com.example.demo;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.catalina.Container;
import org.apache.catalina.Context;
import org.apache.catalina.Executor;
import org.apache.catalina.Globals;
import org.apache.catalina.Host;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Server;
import org.apache.catalina.Service;
import org.apache.catalina.Wrapper;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.JasperListener;
import org.apache.catalina.core.NamingContextListener;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.core.StandardEngine;
import org.apache.catalina.core.StandardHost;
import org.apache.catalina.core.StandardServer;
import org.apache.catalina.core.StandardService;
import org.apache.catalina.startup.Constants;
import org.apache.catalina.startup.ContextConfig;
import org.apache.catalina.startup.Tomcat.DefaultWebXmlListener;
import org.apache.coyote.http11.Http11Protocol;

public class DemoMain {
	
	private void printServer(Server server) {
		System.out.println("Server Info: " + server.getInfo());
		Service[] findServices = server.findServices();
		for (Service service : findServices) {
			Connector[] findConnectors = service.findConnectors();
			for (Connector connector : findConnectors) {
				System.out.println("  ConnectorInfo: " + connector.getInfo());
			}

			Executor[] findExecutors = service.findExecutors();
			for (Executor executor : findExecutors) {
				System.out.println(" Executor name " + executor.getName());
			}

			Container container = service.getContainer();
			printContainer(container, "  ");
		}
	}

	private void printContainer(Container container, String indent) {
		System.out.println(indent + " Info: " + container.getInfo());
		System.out.println(indent + " Name: " + container.getName());
		Container[] children = container.findChildren();
		for (Container child : children) {
			printContainer(child, indent + "  ");
		}
	}

	private void enableNaming(Server server) {
		server.addLifecycleListener(new NamingContextListener());

		System.setProperty("catalina.useNaming", "true");

		String value = "org.apache.naming";
		String oldValue = System.getProperty(javax.naming.Context.URL_PKG_PREFIXES);
		if (oldValue != null) {
			if (oldValue.contains(value)) {
				value = oldValue;
			} else {
				value = value + ":" + oldValue;
			}
		}
		System.setProperty(javax.naming.Context.URL_PKG_PREFIXES, value);

		value = System.getProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY);
		if (value == null) {
			System.setProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
		}
	}

	public void start() throws MalformedURLException 
	{
		String appDocBase = new File("src/main/webapp").getAbsolutePath();

		// setup a context
		Context context = new StandardContext();
		context.setName("/");
		context.setPath("/");
		context.setDocBase(appDocBase);
		context.addLifecycleListener(new DefaultWebXmlListener());
		context.setSessionTimeout(30);
		for (int i = 0; i < DEFAULT_MIME_MAPPINGS.length;) {
			context.addMimeMapping(DEFAULT_MIME_MAPPINGS[i++], DEFAULT_MIME_MAPPINGS[i++]);
		}
		context.addWelcomeFile("index.jsp");

		File configFile = new File("src/main/webapp/META-INF/context.xml");
		context.setConfigFile(configFile.toURI().toURL());

		ContextConfig ctxCfg = new ContextConfig();
		ctxCfg.setDefaultWebXml(Constants.NoDefaultWebXml);
		context.addLifecycleListener(ctxCfg);

		Wrapper sw = context.createWrapper();
		context.addChild(sw);
		sw.setServletClass("com.example.demo.ClassLoaderServlet");
		sw.setName("info");
		sw.setLoadOnStartup(1);
		sw.addMapping("/info");
			
		// setup a virtual host.
		Host host = new StandardHost();
		host.setName("localhost");
		host.addChild(context);

		// setup an engine
		String engineBaseDir = new File("basedir").getAbsolutePath();
		System.setProperty(Globals.CATALINA_HOME_PROP, engineBaseDir);
		System.setProperty(Globals.CATALINA_BASE_PROP, engineBaseDir);
		System.out.println("Engine Base dir: " + engineBaseDir);

		StandardEngine engine = new StandardEngine();
		engine.setName("engine");
		engine.setDefaultHost("localhost");
		engine.addChild(host);
		engine.setBaseDir(engineBaseDir);

		// setup a connector
		Connector connector = new Connector("org.apache.coyote.http11.Http11Protocol");
		Http11Protocol http11Protocol = (Http11Protocol) connector.getProtocolHandler();
		http11Protocol.setCompressionMinSize(1024*10);
		connector.setPort(8080);

		// setup a service
		Service service = new StandardService();
		service.setName("Tomcat");
		service.setContainer(engine);
		service.addConnector(connector);

		// setup a server
		Server server = new StandardServer();
		server.setPort(8006);
		server.setShutdown("SHUTDOWN");
		server.addService(service);
		server.addLifecycleListener(new JasperListener());
		enableNaming(server);

		// start the server.
		try {
			server.start();
			printServer(server);
			server.await();
		} catch (LifecycleException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws MalformedURLException {
		DemoMain launcher = new DemoMain();
		launcher.start();
	}

	private static final String[] DEFAULT_MIME_MAPPINGS = { "abs", "audio/x-mpeg", "ai", "application/postscript", "aif", "audio/x-aiff",
			"aifc", "audio/x-aiff", "aiff", "audio/x-aiff", "aim", "application/x-aim", "art", "image/x-jg", "asf", "video/x-ms-asf",
			"asx", "video/x-ms-asf", "au", "audio/basic", "avi", "video/x-msvideo", "avx", "video/x-rad-screenplay", "bcpio",
			"application/x-bcpio", "bin", "application/octet-stream", "bmp", "image/bmp", "body", "text/html", "cdf", "application/x-cdf",
			"cer", "application/pkix-cert", "class", "application/java", "cpio", "application/x-cpio", "csh", "application/x-csh", "css",
			"text/css", "dib", "image/bmp", "doc", "application/msword", "dtd", "application/xml-dtd", "dv", "video/x-dv", "dvi",
			"application/x-dvi", "eps", "application/postscript", "etx", "text/x-setext", "exe", "application/octet-stream", "gif",
			"image/gif", "gtar", "application/x-gtar", "gz", "application/x-gzip", "hdf", "application/x-hdf", "hqx",
			"application/mac-binhex40", "htc", "text/x-component", "htm", "text/html", "html", "text/html", "ief", "image/ief", "jad",
			"text/vnd.sun.j2me.app-descriptor", "jar", "application/java-archive", "java", "text/x-java-source", "jnlp",
			"application/x-java-jnlp-file", "jpe", "image/jpeg", "jpeg", "image/jpeg", "jpg", "image/jpeg", "js", "application/javascript",
			"jsf", "text/plain", "jspf", "text/plain", "kar", "audio/midi", "latex", "application/x-latex", "m3u", "audio/x-mpegurl",
			"mac", "image/x-macpaint", "man", "text/troff", "mathml", "application/mathml+xml", "me", "text/troff", "mid", "audio/midi",
			"midi", "audio/midi", "mif", "application/x-mif", "mov", "video/quicktime", "movie", "video/x-sgi-movie", "mp1", "audio/mpeg",
			"mp2", "audio/mpeg", "mp3", "audio/mpeg", "mp4", "video/mp4", "mpa", "audio/mpeg", "mpe", "video/mpeg", "mpeg", "video/mpeg",
			"mpega", "audio/x-mpeg", "mpg", "video/mpeg", "mpv2", "video/mpeg2", "nc", "application/x-netcdf", "oda", "application/oda",
			"odb", "application/vnd.oasis.opendocument.database", "odc", "application/vnd.oasis.opendocument.chart", "odf",
			"application/vnd.oasis.opendocument.formula", "odg", "application/vnd.oasis.opendocument.graphics", "odi",
			"application/vnd.oasis.opendocument.image", "odm", "application/vnd.oasis.opendocument.text-master", "odp",
			"application/vnd.oasis.opendocument.presentation", "ods", "application/vnd.oasis.opendocument.spreadsheet", "odt",
			"application/vnd.oasis.opendocument.text", "otg", "application/vnd.oasis.opendocument.graphics-template", "oth",
			"application/vnd.oasis.opendocument.text-web", "otp", "application/vnd.oasis.opendocument.presentation-template", "ots",
			"application/vnd.oasis.opendocument.spreadsheet-template ", "ott", "application/vnd.oasis.opendocument.text-template", "ogx",
			"application/ogg", "ogv", "video/ogg", "oga", "audio/ogg", "ogg", "audio/ogg", "spx", "audio/ogg", "flac", "audio/flac", "anx",
			"application/annodex", "axa", "audio/annodex", "axv", "video/annodex", "xspf", "application/xspf+xml", "pbm",
			"image/x-portable-bitmap", "pct", "image/pict", "pdf", "application/pdf", "pgm", "image/x-portable-graymap", "pic",
			"image/pict", "pict", "image/pict", "pls", "audio/x-scpls", "png", "image/png", "pnm", "image/x-portable-anymap", "pnt",
			"image/x-macpaint", "ppm", "image/x-portable-pixmap", "ppt", "application/vnd.ms-powerpoint", "pps",
			"application/vnd.ms-powerpoint", "ps", "application/postscript", "psd", "image/vnd.adobe.photoshop", "qt", "video/quicktime",
			"qti", "image/x-quicktime", "qtif", "image/x-quicktime", "ras", "image/x-cmu-raster", "rdf", "application/rdf+xml", "rgb",
			"image/x-rgb", "rm", "application/vnd.rn-realmedia", "roff", "text/troff", "rtf", "application/rtf", "rtx", "text/richtext",
			"sh", "application/x-sh", "shar", "application/x-shar",
			/* "shtml", "text/x-server-parsed-html", */
			"sit", "application/x-stuffit", "snd", "audio/basic", "src", "application/x-wais-source", "sv4cpio", "application/x-sv4cpio",
			"sv4crc", "application/x-sv4crc", "svg", "image/svg+xml", "svgz", "image/svg+xml", "swf", "application/x-shockwave-flash", "t",
			"text/troff", "tar", "application/x-tar", "tcl", "application/x-tcl", "tex", "application/x-tex", "texi",
			"application/x-texinfo", "texinfo", "application/x-texinfo", "tif", "image/tiff", "tiff", "image/tiff", "tr", "text/troff",
			"tsv", "text/tab-separated-values", "txt", "text/plain", "ulw", "audio/basic", "ustar", "application/x-ustar", "vxml",
			"application/voicexml+xml", "xbm", "image/x-xbitmap", "xht", "application/xhtml+xml", "xhtml", "application/xhtml+xml", "xls",
			"application/vnd.ms-excel", "xml", "application/xml", "xpm", "image/x-xpixmap", "xsl", "application/xml", "xslt",
			"application/xslt+xml", "xul", "application/vnd.mozilla.xul+xml", "xwd", "image/x-xwindowdump", "vsd", "application/vnd.visio",
			"wav", "audio/x-wav", "wbmp", "image/vnd.wap.wbmp", "wml", "text/vnd.wap.wml", "wmlc", "application/vnd.wap.wmlc", "wmls",
			"text/vnd.wap.wmlsc", "wmlscriptc", "application/vnd.wap.wmlscriptc", "wmv", "video/x-ms-wmv", "wrl", "model/vrml", "wspolicy",
			"application/wspolicy+xml", "Z", "application/x-compress", "z", "application/x-compress", "zip", "application/zip" };
}
